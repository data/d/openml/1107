# OpenML dataset: tumors_C

https://www.openml.org/d/1107

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Embryonal tumours of the central nervous system

Prediction of Central Nervous System Embryonal Tumour Outcome based on Gene Expression.
Nature, VOL 415, pp. 436-442, 24 January 2002.

Scott L. Pomeroy, Pablo Tamayo, Michelle Gaasenbeek, Lisa M. Sturla, Michael Angelo, Margaret E. McLaughlin, John Y. H. Kim, Liliana C. Goumnerova, Peter M. Black, Ching Lau, Jeffrey C. Allen, David Zagzag, James M. Olson, Tom Curran, Cynthia Wetmore, Jaclyn A. Biegel, Tomaso Poggio, Shayan Mukherjee, Ryan Rifkin, Andrea Califano, Gustavo Stolovitzky, David N. Louis, Jill P. Mesirov, Eric S. Lander & Todd R. Golub

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1107) of an [OpenML dataset](https://www.openml.org/d/1107). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1107/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1107/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1107/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

